\documentclass{beamer}

%
% Choose how your presentation looks.
%
% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
%
\mode<presentation>
{
  \usetheme{Singapore}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{seahorse} % or try albatross, beaver, crane, ...
  \usefonttheme{serif}  % or try serif, structurebold, ...
  \setbeamerfont{section in head/foot}{family=\sffamily}
%   \setbeamertemplate{navigation symbols}{}
  \usenavigationsymbolstemplate{}
  \setbeamertemplate{caption}[numbered]
  \setbeamercolor{itemize item}{fg = red!60!black}
  \setbeamertemplate{footline}[frame number]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[]{bbding}  % for \HandRight symbol
\usepackage{graphics}

\usepackage{xcolor, colortbl}	% for coloring in table

\usepackage{mdframed}

\usepackage{algpseudocode}

\usepackage{tikz}
\usetikzlibrary{calc, arrows, positioning, shapes, backgrounds, fit}
\usepackage{tikzscale}
\usepackage{adjustbox}

% special numbering for backup slides
\newcommand{\beginbackup}
{
   \newcounter{framenumbervorappendix}
   \setcounter{framenumbervorappendix}{\value{framenumber}}
}
\newcommand{\backupend}
{
   \addtocounter{framenumbervorappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumbervorappendix}} 
}

% cite like ABD@JACM'95
% #1: authors; #2: conference; #3: year
\newcommand{\tinycite}[3]
{
	\textcolor{blue}{{\scriptsize [#1@#2'#3]}}
}

\newcommand{\inputtikz}[2] % #1: size; #2: tikz file (without prefix: tex(tikz))
{
    \begin{center}
    \adjustbox{max width = #1\textwidth}
      {
        \input{tex/#2}
      }
    \end{center}
}

\title{Achieving Low-Latency while Preserving Almost Strong
Consistency in Distributed Storage Services}
\author{Hengfeng Wei}
\institute{ICS, NJU}
\date{\today}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
\AtBeginSubsection[]
{
	\begin{frame}<beamer>
		\frametitle{Outline}
		\tableofcontents[currentsection]
	\end{frame}
}

% \AtBeginSection[]
% {
% 	\begin{frame}<beamer>
% 		\frametitle{Outline}
% 		\tableofcontents[currentsection]
% 	\end{frame}
% }
%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
	\frametitle{Outline}
	\tableofcontents
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\subsection{Motivation}
%%%%%%%%%%%%%
\begin{frame}{Distributed Storage Service Hidden Everywhere}
  \begin{figure}[htp]
    \centering
    \includegraphics[width = 0.85\textwidth]{fig/applications.pdf}
  \end{figure}
  
  \vspace{0.10cm}
  Facebook \tinycite{Facebook Inc}{SIGOPS OSR}{10} \tinycite{Facebook
  Inc}{OSDI}{10}

  \begin{itemize}
  {\small
    \item \textcolor{blue}{High traffic:} serving hundreds of millions users at
    perk
    \item \textcolor{blue}{Big data:} producing over one million images at perk
    \item \textcolor{blue}{Distributed system:} tens of thousands of
    servers}
  \end{itemize}
  
  \vspace{0.20cm}
  \textcolor{red}{Big challenge on underlying distributed storage service
  $\ldots$}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Desired Properties of Distributed Storage Service}
  Three-high and one-low properties:\\
  \tinycite{Amazon}{SOSP}{07} \tinycite{Yahoo!}{VLDB}{08} \tinycite{Facebook
  Inc}{SIGOPS OSR}{10} \tinycite{Google}{OSDI}{12}
  
  \vspace{0.10cm}
  
  \begin{itemize}
    \setlength{\itemsep}{5pt}
    \item High availability
      \begin{itemize}
        \item 99.9\% = 10.08 minutes downtime per week
	  \end{itemize}
    \item High fault-tolerance
      \begin{itemize}
        \item failures are the norm rather than the exception
      \end{itemize}
    \item High scalability
      \begin{itemize}
        \item grow (or shrink) on demand
      \end{itemize}
    \item Low latency
      \begin{itemize}
        \item the sooner the better
      \end{itemize}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Distributed Storage Service}
  \begin{figure}[htp]
    \centering
    \includegraphics[width = 0.85\textwidth]{fig/storages.pdf}
  \end{figure}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Replication Inside Distributed Storage Service}
%   \begin{columns}
%     \column{0.60\textwidth}
% 	  \begin{figure}[htp]
% 	    \centering
% 	    \includegraphics[width = 0.90\textwidth]{fig/replication.png}
% 	  \end{figure}
%     \column{0.40\textwidth}
%       Replication technique for
%       \begin{itemize}
%         \item High availability
%         \item Low latency
%         \item High fault-tolerance
%         \item High scalability
%       \end{itemize}
%   \end{columns}

  \begin{figure}[htp]
    \centering
    \includegraphics[width = 1.0\textwidth]{fig/geo-replication.png}
  \end{figure}
  
  \begin{center}
	{\small Replication technique towards the ``three-high and one-low''
	properties (picture from \tinycite{Lloyd}{CACM}{14}).}
  \end{center}
  
  \vspace{0.30cm}
  \textcolor{red}{At the expense of introducing data consistency problems
  $\ldots$}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Data Consistency Problem Coming with Replication}
  Inconsistency means strange behaviors to users.  
  
  \vspace{0.10cm}

  \begin{figure}[htp]
    \centering
    \includegraphics[width= 0.65\textwidth]{fig/data_inconsistency_postblog.pdf}
  \end{figure}
  
%   \begin{center}
%   {\small  Not a big deal: refresh until it \textcolor{blue}{\emph{eventually}}
%     appears. }
%   \end{center}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Another Desired Property: Data Consistency}
    Consistency model \tinycite{Steinke}{JACM}{04}: 
      \begin{itemize}
        \item \textcolor{blue}{formally} specifying what is (\emph{not}) allowed
		\item providing different \textcolor{blue}{levels} of consistency
	  \end{itemize}
	  
	\vspace{0.30cm}
	
    Stronger consistency is desired:
      \begin{itemize}
        \item users feel better about their data
        \item easier for developers to program
      \end{itemize}
\end{frame}
%%%%%%%%%%%%%
% \begin{frame}{Another Desired Property: Consistency}
%   \begin{figure}[htp]
%     \centering
%     \includegraphics[width =
%     0.75\textwidth]{fig/consistency_model_explained.pdf}
%   \end{figure}
% \end{frame}
%%%%%%%%%%%%%
\begin{frame}{Desired Properties Summarized}
  \begin{columns}
    \column{0.40\textwidth}
      Three-high \& one-low:
      {\small 
      \begin{itemize}
        \item High availability
        \item Low latency
        \item High fault-tolerance
        \item High scalability
      \end{itemize}
      }
    \column{0.30\textwidth}
      \uncover<2->
      {
        \textcolor{teal}{\textsc{Tradeoff}}
	    \begin{figure}[htp]
	      \centering
	      \includegraphics[width = 1.00\textwidth]{fig/tradeoff}
	    \end{figure}      
      }
    \column{0.35\textwidth}
      The fifth property:
      \begin{itemize}
        \item Data consistency
      \end{itemize}
  \end{columns}
 
  \uncover<3->
  {
  \begin{figure}[htp]
	\centering
	\includegraphics[width = 0.70\textwidth]{fig/consistency_4properties_tradeoff.pdf}
  \end{figure}
  } 
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Consistency/Latency Tradeoff}
    Latency is crucial:
      \begin{itemize}
        \item  \emph{increasing latency 100 to 400ms reduces the daily
        number of searches per user by 0.2\% to 0.6\%} {\scriptsize
        \textcolor{blue}{[Google'09]}}
      \end{itemize}
    
    \vspace{0.30cm}
    
    Latency is dominated by \textcolor{blue}{network delay}:
%      {\footnotesize RAM $\ll$ disk $\ll$ network delay [wide-area or wireless]}
      \begin{itemize}
        \item measured by number of \textcolor{red}{\emph{round-trips}}
	    \begin{figure}[htp]
	      \centering
	      \includegraphics[width = 0.80\textwidth]{fig/latency_round.pdf}
	    \end{figure}            
      \end{itemize}
  
  \pause
  
  \begin{mdframed}[leftmargin = 35pt, rightmargin = 35pt]
    \centering
    \textcolor{blue}{low latency $\triangleq$ single round-trip}
  \end{mdframed}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Consistency/Latency Tradeoff}
  The state-of-the-art of ``consistency/latency tradeoff'':
  
  \pause
  \begin{itemize}[<+->]
    \setlength{\itemsep}{12pt}
    \item Theorectically, we have the \textcolor{red}{impossibility result:} 
      \begin{mdframed}
 		{\small Strong consistency (\emph{atomicity}) cannot be achieved with
 		low latency \textcolor{gray}{and optimal fault-tolerance}
 		\tinycite{Dutta}{PODC}{04}.}
      \end{mdframed}
    \item Practically, consistency is sacrificed for low latency:
    \vspace{0.10cm}
    {\small
      \begin{table}
		\begin{tabular}{c|c}
		  \hline
		  Amazon DynamoDB & Eventual consistency \\ \hline
		  Yahoo! PNUTS & Time-line consistency  \\ \hline
		  Facebook Cassandra & Tunable consistency \\ \hline
		  LinkedIn Voldemort & Tunable consistency \\ \hline
		\end{tabular}
	  \end{table}
	}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Problem Statement}
%%%%%%%%%%%%%
\begin{frame}{Problem Statement}
  Generally, can we do better in ``consistency/latency tradeoff''?
  
  \vspace{0.40cm}
  More specifically, 
  \begin{mdframed}[linecolor = blue]
    How to achieve \textcolor{blue}{low-latency} while
    preserving {\large \texttt{\textcolor{red}{ALMOST}}} \textcolor{blue}{strong
    consistency}?
  \end{mdframed}
  
%   \pause
%   \vspace{0.70cm}
%   More fundamentally (ultimate goal),
%   \begin{mdframed}[linecolor = red]
%     What level of consistency can be achieved with low latency?
%   \end{mdframed} 
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Overview of Our Solution}
  \begin{mdframed}[linecolor = blue]
    How to achieve \textcolor{blue}{low-latency} while
    preserving {\large \texttt{\textcolor{red}{ALMOST}}} \textcolor{blue}{strong
    consistency}?
  \end{mdframed}
  
  \vspace{0.50cm}
  \begin{itemize}
    \setlength{\itemsep}{8pt}
    \item \textcolor{blue}{Low latency:} requests complete in a single
    round-trip
    \item \textcolor{blue}{Strong consistency:} atomicity
    \item \texttt{\textcolor{red}{ALMOST:}}
      \begin{itemize}
        \item deterministically bounded staleness
        \item probabilistically quantifying the occurrence of ``reading stale
        values"
      \end{itemize}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preliminaries}

%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{System Architecture}

%%%%%%%%%%%%%
\begin{frame}{System Architecture}
  \begin{figure}
    \centering
    \includegraphics[width = 0.60\textwidth]{fig/client_server.pdf}
  \end{figure}
  
  \vspace{0.20cm}
  Two roles of nodes:
  \begin{itemize}
    \item any number of clients
    \item fixed number of server replicas 
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Typical Scenarios of System Architecture}
  \begin{columns}
    \column{0.50\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width = 0.80\textwidth]{fig/system_arch_internet.pdf}
      \end{figure}
      \begin{center}
        {\small Wide-area applications in the Internet
        \textcolor{blue}{\scriptsize [Amazon, Google+]}.}
      \end{center}
    \pause
    \column{0.50\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width = 0.85\textwidth]{fig/system_arch_p2p.pdf}
      \end{figure}
      \begin{center}
        {\small Mobile phones in P2P network
        \textcolor{blue}{\scriptsize [adopted in our experiments]}.}
      \end{center}
  \end{columns}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Executions}
  \inputtikz{0.90}{client_server_execution}
  \vspace{0.10cm}
  Execution: clients' view of a run of distributed storage system
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Atomicity Review}
%%%%%%%%%%%%%
\begin{frame}{Atomicity Review}
  \begin{center}
    \emph{\small A consistency semantic is a \texttt{test} on an execution.}
  \end{center}
  
  \uncover<2->
  {
  \begin{mdframed}[linecolor = blue]
%     {\small Atomicity simulates an execution on a single replica from that
%     on multiple ones.}
    {\small Atomicity requires a ``replicated system" behave as if each data
    has only one copy.}
  \end{mdframed}
  }
  
  \vspace{0.20cm}
  
  \begin{columns}[c]
    \column{0.50\textwidth}
      \uncover<2->
      {
      \begin{figure}[htp]
       \centering
       \includegraphics[width =
       0.85\textwidth]{fig/atomicity_simulation.pdf}
      \end{figure}
      }
    \column{0.60\textwidth}
    \uncover<3->
    {\small
      An execution satisfies \emph{atomicity} \texttt{iff} \\ it is possible to
      \textcolor{blue}{total order} all its operations while meeting
          \begin{itemize}
            \item \textcolor{purple}{[semantics requirement]} 
            
				each \textsc{read} reads from the latest \textsc{write} on the same
				variable.
            \item \textcolor{purple}{[real-time requirement]}
            
              respecting the $\prec$ partial order.
          \end{itemize}
    }
  \end{columns}
  
    \vspace{0.20cm}
    \begin{center}
      \emph{\small A system satisfies a consistency if all its executions do.}
    \end{center}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Almost Strong Consistency}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Achieving Almost Strong Consistency}

%%%%%%%%%%%%%
\begin{frame}{Almost Strong Consistency}

  Almost strong consistency: 
  \begin{itemize}
    \item \HandRight \hspace{5pt} \textcolor{blue}{deterministically bounded
    staleness}
    \item {\scriptsize probabilistically quantifying the occurrence of ``reading
    stale values''}
  \end{itemize}
  
  \vspace{0.50cm}
  
  Specifically, it is \emph{2-atomicity}:
  
  \begin{mdframed}[linecolor = teal]
    \begin{center}
      \textcolor{purple}{Modified semantics requirement:}
    \end{center} 
    
    each \textsc{read} reads from \textcolor{red}{one of
    the 2 latest} \textsc{write}s on the same variable.
  \end{mdframed}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{System model \tinycite{ABD}{JACM}{95}}
  \begin{itemize}
    \setlength{\itemsep}{15pt}
    \item \textcolor{blue}{Timing model:} \hfill
      \begin{itemize}
        \item no \emph{real} global time
      \end{itemize}
    \item \textcolor{blue}{Failure model:} 
      \begin{itemize}
        \item arbitrary number of clients can crash
        \item at least a majority of replicas will not crash
      \end{itemize}
    \item \textcolor{blue}{Communication model:} 
      \begin{itemize}
        \item messages can be delayed, lost, or delivered out of order
        \item messages cannot be altered or corrupted
	  \end{itemize}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Key-Value Data Model with Single Writer}
  Key-value data model:
      \begin{itemize}
        \item \texttt{read(key:String)}
        \item \texttt{write(key:String, val)}
      \end{itemize}
      
  \vspace{0.30cm}
  
  Supporting single writer per key \textcolor{blue}{\small (not
  to the whole system)}
      \begin{itemize}
	    \item sessions, profiles
	    \item blog publishing, picture sharing
	  \end{itemize}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Algorithms for 2-Atomicity} 

  	  \begin{figure}[htp]
	    \centering
	    \includegraphics[width =
	    0.80\textwidth]{fig/SW_2atomicity_write_seq_diagram.pdf}
	  \end{figure}
	  
	\begin{center}
	  \texttt{Write} operation of 2-atomicity algorithm.
	\end{center}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{}
%   	  \begin{figure}[htp]
% 	    \centering
% 	    \includegraphics[width =
% 	    0.80\textwidth]{fig/SW_2atomicity_read_seq_diagram.pdf}
% 	  \end{figure}
  \inputtikz{0.80}{SW_2atomicity_read_seq_diagram}
	\begin{center}
	  \texttt{Read} operation of 2-atomicity algorithm.
	\end{center}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Correctness}
  Basically, it is a \textcolor{blue}{\emph{case-by-case analysis}} concerning
  the partial order and semantics of \texttt{read}/\texttt{write} operations.
  
  \vspace{0.50cm}
  
  \begin{mdframed}
    It achieves 2-atomicity (i.e., \textcolor{blue}{deterministically} bounded
    staleness $\le 2$ for each \texttt{read}).
  \end{mdframed}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Quantifying Almost Strong Consistency}

%%%%%%%%%%%%%
\begin{frame}{Quantifying Almost Strong Consistency}
  Almost strong consistency: 
  \begin{itemize}
    \item {\scriptsize deterministically bounded staleness}
    \item \HandRight \hspace{5pt} \textcolor{blue}{probabilistically
    quantifying the occurrence of ``reading stale values''}
  \end{itemize}
  
  \vspace{0.50cm}
  
  \begin{mdframed}
    {\small \textcolor{blue}{How often} will my data store be strong consistent
    \textcolor{blue}{under} a given configuration and workload?}
  \end{mdframed}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Occurrences of ``reading 2-atomicity''}
  The case for \texttt{read} operation \texttt{r} to get
  \textcolor{blue}{\emph{2-stale}} value:
  
  \vspace{0.50cm}
  \begin{columns}
    \column{0.50\textwidth}
	  \begin{figure}[htp]
	    \centering
	    \includegraphics[width = 0.90\textwidth]{fig/2atomicity_case.pdf}
	  \end{figure}
    \column{0.60\textwidth}
      \begin{itemize}
        \setlength{\itemsep}{5pt}
        \item $\exists w: r_{st} \in [w_{st}, w_{ft}]$ {\scriptsize (not $r ||
        w$)}
		\item $\exists w' \prec w$
		\item $\exists r': r'_{ft} \in [w_{st}, r_{st}]$ {\scriptsize (not $r' \prec
		r$)}
		\vspace{0.30cm}
		\item $r = R(w')$
		\item $r' = R(w)$
      \end{itemize}
  \end{columns}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Quantifying 2-Atomicity}
  \begin{columns}
    \column{0.50\textwidth}
      \begin{figure}[htp]
	    \centering
	    \includegraphics[width = 0.80\textwidth]{fig/concurrency_pattern_easyedition.pdf}
	  \end{figure}
	  \begin{center}
      Concurrency pattern (\texttt{CP}):
      \begin{itemize}
	    \item $\exists w: r_{st} \in [w_{st}, w_{ft}]$
		\item $\exists w' \prec w$
		\item $\exists r': r'_{ft} \in [w_{st}, r_{st}]$
	  \end{itemize}
	  \end{center}
	\column{0.50\textwidth}
      \begin{figure}[htp]
	    \centering
	    \includegraphics[width = 0.75\textwidth]{fig/old_new_inversion_quorum.pdf}
	  \end{figure}
	  \begin{center}
	  Old-new inversion (\texttt{ONI}):
	  \begin{itemize}
		\item $r = R(w')$
		\item $r' = R(w)$
	  \end{itemize}
	  \end{center}
  \end{columns} 
  
  \pause
  \vspace{0.30cm}
  
  \[
    \mathbb{P}(\texttt{2-atomicity}) = \mathbb{P} (\texttt{CP} \cap
    \texttt{ONI}) = \mathbb{P}(\texttt{CP}) \cdot \mathbb{P}(\texttt{ONI} \mid
    \texttt{CP}).
  \] 
  
  \begin{mdframed}
    (As) Expected: The cases of reading \emph{2-stale} values
    are \textcolor{blue}{probabilistically} rare.
  \end{mdframed}
\end{frame}
%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experimental Results}

\subsection{Implementations}
%%%%%%%%%%%%%
\begin{frame}{Implementations}
  \begin{columns}
    \column{0.50\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width = 0.85\textwidth]{fig/system_arch_p2p.pdf}
      \end{figure}
      \begin{center}
        {\small Mobile phones in P2P network.}
      \end{center}    
    \column{0.50\textwidth}
      Implementing key-value stores:
	  \begin{itemize}
	    \item Key: \texttt{String}
	    \item Val: \texttt{String text}
	  \end{itemize}      
  \end{columns}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Evaluations}
%%%%%%%%%%%%%
\begin{frame}{Why to Do Experimental Evaluations}
%   \begin{mdframed}
%     Achieving \textcolor{blue}{low-latency} while preserving
%     \textcolor{blue}{almost strong consistency}.
%   \end{mdframed}
  
%   \begin{itemize}
%     \item Latency: sensitive to configuration and network
%     \item $\mathbb{P}(\texttt{2-atomicity})$
%       \begin{itemize}
%         \item concurrency pattern (\texttt{CP}): \\ \hspace{0.50cm} sensitive to
%         workload and network
%         \item old-new inversion (\texttt{ONI}): \\ \hspace{0.50cm} sensitive to
%         configuration and network
%       \end{itemize}
%   \end{itemize}
  
%   \vspace{0.50cm}
  The theory concerning both latency and $\mathbb{P}(\texttt{2-atomicity})$
  relies on the model adopted.
  
  \vspace{0.30cm}
  \begin{mdframed}[linecolor = blue]
    How well does my data store behave under given workload right now?
  \end{mdframed}  
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Metrics and Parameters}

  \begin{table}
	\begin{tabular}{c|c|c|c}
	  \cline{2-3}
	     	& \multicolumn{2}{c|}{\cellcolor{blue!25}workload}  \\
	     	\cline{2-3}
	  \cellcolor{red!50}Metrics/Parameters & \cellcolor{blue!25}ops &
	  		\cellcolor{blue!25}rate & \cellcolor{blue!25}replica factor \\ \hline
	  \cellcolor{blue!25}delay & & & \\
	  		\hline \cellcolor{blue!25}$\mathbb{P} (\texttt{CP})$ & & & \\ \hline
	  \cellcolor{blue!25}$\mathbb{P} (\texttt{2-Atomicity})$ & & & \\ \hline
	\end{tabular}
  \end{table}
  
  \vspace{0.30cm}
  Remarks on workload:
  \begin{itemize}
    \item on a single \texttt{key}
    \item values are ignored (\textcolor{red}{\texttt{string text}} vs.
    \textcolor{red}{\texttt{picture}})
    \item \texttt{r/w} ratio 
  \end{itemize}
\end{frame}
%%%%%%%%%%%
\begin{frame}{Metric: Latency}
  \begin{figure}[htp]
    \includegraphics[width = 1.0\textwidth]{fig/exp_latency.pdf}
  \end{figure}
\end{frame}
%%%%%%%%%%%
\begin{frame}{Metric: $\mathbb{P}(\texttt{CP})$ and
$\mathbb{P}(\texttt{2-Atomicity})$} 
\begin{figure}[htp]
    \includegraphics[width = 1.0\textwidth]{fig/exp_quantification.pdf}
  \end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related Work}

%%%%%%%%%%%%%
\begin{frame}{Related Work}
    Consistency/latency tradeoff: 
      \begin{itemize}
        \item from CAP \tinycite{Brewer}{PODC}{00} to PACE\textcolor{red}{LC}
        \tinycite{Abadi}{Computer}{12}
	  \end{itemize}
	
	\pause
	\vspace{0.20cm}    
    Complexity of implementing atomicity:
	  \begin{itemize}
	    \item \texttt{read} in 2-rounds in \tinycite{ABD}{JACM}{95}
	    \item best-case (single round) complexity \tinycite{Guerraoui}{PODC}{07}
	  \end{itemize}
	
	\pause
	\vspace{0.20cm}
    Quantifying consistency:
      \begin{itemize}
        \item bounded staleness 
          \begin{itemize}
            \item \tinycite{Aiyer}{DISC}{05}: still 2-rounds for 2-atomicity
            \item \tinycite{Henzinger}{POPL}{13}: on queue and stack
          \end{itemize}
        \item probabilistic analysis on \emph{regular} consistency \\
        \tinycite{Welch}{DC}{05}
        \tinycite{Bailis}{VLDB}{12}
      \end{itemize}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Conclusion and Future Work}
  \begin{mdframed}[linecolor = blue]
    To achieve \textcolor{blue}{low-latency} while
    preserving \textcolor{blue}{almost strong consistency}.
  \end{mdframed}
  
  \vspace{0.30cm}
    Low latency $\triangleq$ single round-trip
    
	Almost strong consistency:
      \begin{itemize}
        \item deterministically bounded staleness
        \item probabilistically quantifying cases of ``reading stale
        values"
      \end{itemize}
  
  \pause
  \vspace{0.50cm}
  Future work:
  \begin{itemize}
    \item analyzing the case of multiple writers
    \item more involved (\emph{and richer}) distributed data structures
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\beginbackup
%%%%%%%%%%%%%
\begin{frame}
  \begin{figure}[htp]
	\centering
	\includegraphics[width = 0.618\textwidth]{fig/thankyou.jpg}
  \end{figure}
	  
  \vspace{0.30cm}  
  
  \begin{center}
    {\Huge \texttt{Backup ...}}
  \end{center}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Data Consistency Problem Coming with Replication}
  Inconsistency means strange behaviors to users.
  
  \vspace{0.10cm}
  
  \begin{columns}
    \column{0.50\textwidth}
	  \begin{figure}[htp]
	    \centering
	    \includegraphics[width =
	    0.95\textwidth]{fig/data_consistency_causal.png}
	  \end{figure}
	    {\small Conversation messages are \textcolor{blue}{\emph{causally}}
	    related in one replica.}
	     
    \column{0.50\textwidth}
	  \begin{figure}[htp]
	    \centering
	    \includegraphics[width =
	    1.00\textwidth]{fig/data_inconsistency_causal.png}
	  \end{figure}     
	    {\small A different story in another replica due to message disorder.}
  \end{columns}

  \vspace{0.50cm}
  \begin{center}
  {\small It is annoying. I don't want this.}
  \end{center}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Data Consistency Problem Coming with Replication}
  Inconsistency means strange behaviors to users.
  
  \vspace{0.10cm}
	  
  \begin{figure}[htp]
	\centering
	\includegraphics[width = 0.50\textwidth]{fig/data_inconsistency_atm.pdf}
  \end{figure}  
  
%   \begin{center}
%     {\small This should not happen. \textcolor{blue}{\emph{Transaction}} is
%     required.}
%   \end{center}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Key-Value Data Model}
  \begin{columns}
    \column{0.50\textwidth}
      \begin{itemize}
        \item \texttt{read(key:String)}
        \item \texttt{write(key:String, val)}
      \end{itemize}
      \begin{figure}[htp]
        \centering
        \includegraphics[width = 0.85\textwidth]{fig/kvmodel}
      \end{figure}
    \column{0.50\textwidth}
      Popular applications:
      \begin{itemize}
        \item sessions, configurations
        \item online shopping cart
        \item profile, blog, picuture
      \end{itemize}
      
      \begin{figure}[htp]
        \centering
        \includegraphics[width = 0.85\textwidth]{fig/Riak}
      \end{figure}
  \end{columns}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Algorithms for 2-Atomicity} 
  \begin{itemize}
    \setlength{\itemsep}{10pt}
    \item \texttt{write(key, val):}
      \begin{itemize}
        \item increment \texttt{version} for this \texttt{key}
        \item contact a majority of replicas:
          \begin{itemize}
            \item send \texttt{(key, val, version)} to all replicas
            \item \textcolor{teal}{each replica store \texttt{(key, val,
            version)} if \texttt{version} is larger}
            \item wait for \texttt{ACK}s from a majority of replicas
          \end{itemize}
        \item return \texttt{ACK}
      \end{itemize}

    \pause

    \item \texttt{read(key):}
      \begin{itemize}
        \item contact a majority of replicas:
          \begin{itemize}
            \item send \texttt{(key)} to all replicas
            \item \textcolor{teal}{each replica responses with its \texttt{(key,
            value, version)}}
            \item wait for \texttt{(key, value, version)} from a majority
            of replicas
          \end{itemize}
        \item return \texttt{val} with the largest \texttt{version}
      \end{itemize}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Algorithm for 2-Atomicity}
  \inputtikz{0.80}{comm}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Algorithm for 2-Atomicity}
	\begin{columns}
	  \column{0.50\textwidth}
	    \begin{algorithmic}[0]
		  \Procedure{\textsc{Write}}{key}
		    \State {\bf inc} \textup{version}
		    \State \Call{Comm}{key:version}
		  \EndProcedure
		  \end{algorithmic}	
	  \column{0.50\textwidth}
		  \begin{algorithmic}[0]
		  	\Procedure{\textsc{Read}}{key}
		      \State \Call{Comm}{key}
		      \State \Return largest version
		  	\EndProcedure
		  \end{algorithmic}	
	\end{columns}  
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Correctness}
  \begin{figure}[htp]
    \centering
    \includegraphics[width = 1.0\textwidth]{fig/correctness_horizontal.pdf}
  \end{figure}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Quantifying Almost Strong Consistency}
  Two mechanisms for quantifying a certain consistency \tinycite{Bailis}{ACM
  Queue}{13}:
  \begin{itemize}
    \setlength{\itemsep}{8pt}
    \item Measurement (\emph{in practice})
      \begin{mdframed}
        {\small How consistent \emph{is} my store under my given workload right
        now?}
      \end{mdframed}
    \item Prediction (\emph{in theory})
      \begin{mdframed}
        {\small How consistent \emph{will} my store be under a given
        configuration and workload?}
      \end{mdframed}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Step 1: Concurrency Pattern}
  \begin{columns}
    \column{0.60\textwidth}
	  \begin{figure}[htp]
	    \centering
	    \includegraphics[width = 0.90\textwidth]{fig/mconcurrency.pdf}
	  \end{figure}
	  
	  \pause
	  \vspace{0.20cm}
	  
	  \begin{figure}[htp]
	    \centering
	    \includegraphics[width = 0.85\textwidth]{fig/analyticalmodel.pdf}
	  \end{figure}
    \column{0.60\textwidth}
      \pause
      
      {\small
	  Justification for the statistical model:
	  \begin{itemize}
	    \item prediction for reference
	    \pause
	    \item statistical models as benchmarks \tinycite{Yahoo!}{SoCC}{10} \\
	    \tinycite{YCSB++}{SoCC}{11}
	    \item statistical models of \emph{real} workload
	    \tinycite{Arlitt}{Networking}{97} \tinycite{Facebook}{SIGMETRICS}{12}
	    \tinycite{Stoica}{VLDB}{13}
	    \pause
	    \item statistical models are used in \tinycite{Kraska}{VLDB}{09} \\
	    \tinycite{Wang}{DSN}{13}
	  \end{itemize}
	  }
  \end{columns}
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Step 2: Old-New Inversion}
  \begin{columns}
    \column{0.60\textwidth}
	  \begin{figure}[htp]
	    \centering
	    \includegraphics[width = 0.85\textwidth]{fig/old_new_inversion.pdf}
	  \end{figure}
	  
	  \pause
	  \vspace{0.20cm}
	  
	  \begin{figure}[htp]
	    \centering
	    \includegraphics[width = 0.80\textwidth]{fig/old_new_inversion_quorum.pdf}
	  \end{figure}
    \column{0.60\textwidth}
      \pause

	  {\small
      Statistical models:
      \begin{itemize}
        \item read/write operating \emph{independently}
        \item each request contacting half of replicas \emph{randomly}
        \item concurrent requests arriving \emph{equally likely} at one
        particular replica in any order
      \end{itemize}
      }
  \end{columns}  
\end{frame}
%%%%%%%%%%%%%
\begin{frame}{Why to Do Experimental Evaluations}
  \begin{mdframed}
    Achieving \textcolor{blue}{low-latency} while preserving
    \textcolor{blue}{almost strong consistency}
  \end{mdframed}
  
  \begin{itemize}
    \item Latency: sensitive to configuration and network
    \item $\mathbb{P}(\texttt{2-atomicity})$
      \begin{itemize}
        \item concurrency pattern (\texttt{CP}): \\ \hspace{0.50cm} sensitive to
        workload and network
        \item old-new inversion (\texttt{ONI}): \\ \hspace{0.50cm} sensitive to
        configuration and network
      \end{itemize}
  \end{itemize}
  
  \vspace{0.30cm}
  
  Theory cannot model them accurately.
\end{frame}
%%%%%%%%%%%
\backupend
%%%%%%%%%%%
\end{document}
